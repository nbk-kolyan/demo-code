<?php

namespace App\Resources\Payment;

use App\Models\PaymentType;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PaymentType
 */
class PaymentTypeResource extends JsonResource
{
    /**
     * {@inheritDoc}
     *
     * @return array<mixed>
     */
    public function toArray($request): array
    {
        return [
            'id'              => $this->id,
            'name'            => $this->name,
            'short_name'      => $this->short_name,
            'pending_message' => $this->pending_message,
            'success_message' => $this->success_message,
            'reject_message'  => $this->reject_message,
        ];
    }
}
