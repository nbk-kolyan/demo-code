<?php

namespace App\Resources\Payment;

use App\Models\PaymentSubAccount;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PaymentSubAccount
 */
class PaymentSubAccountResource extends JsonResource
{
    /**
     * {@inheritDoc}
     *
     * @return array<mixed>
     */
    public function toArray($request = null): array
    {
        return [
            'id'             => $this->id,
            'payment_id'     => $this->payment_id,
            'sub_account_id' => $this->sub_account_id,
            'amount'         => $this->amount,
        ];
    }
}
