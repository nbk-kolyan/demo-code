<?php

namespace App\Resources\Payment;

use App\Models\ConvenienceFee;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin ConvenienceFee
 */
class ConvenienceFeeResource extends JsonResource
{
    /**
     * {@inheritDoc}
     *
     * @return array<mixed>
     */
    public function toArray($request = null): array
    {
        return [
            'id'                   => $this->id,
            'account_id'           => $this->account_id,
            'payment_id'           => $this->payment_id,
            'payment_method_id'    => $this->payment_method_id,
            'amount'               => $this->amount,
            'timestamp'            => $this->timestamp->toDateTimeString(),
            'order_id'             => $this->order_id,
            'auth_code'            => $this->auth_code,
            'comment'              => $this->comment,
            'merchant_id'          => $this->merchant_id,
            'payment_processor_id' => $this->payment_processor_id,
        ];
    }
}
