<?php

namespace App\Resources\Payment;

use App\Models\Payment;
use App\Resources\AbstractResource;
use App\Resources\WithRelations;

/**
 * @mixin Payment
 */
class PaymentResource extends AbstractResource
{
    use WithRelations;

    /**
     * {@inheritDoc}
     *
     * @return array<mixed>
     */
    public function toArray($request = null): array
    {
        return [
            'id'                   => $this->id,
            'account_id'           => $this->account_id,
            'payment_plan_id'      => $this->payment_plan_id,
            'amount'               => $this->amount,
            'timestamp'            => $this->timestamp->toDateTimeString(),
            'order_id'             => $this->order_id,
            'auth_code'            => $this->auth_code,
            'comment'              => $this->comment,
            'merchant_id'          => $this->merchant_id,
            'payment_processor_id' => $this->payment_processor_id,
            'name'                 => $this->name,
            'address'              => $this->addr,
            'city'                 => $this->city,
            'state'                => $this->state,
            'zip'                  => $this->zip,
            'country'              => $this->country,
            'phone'                => $this->phone,
            'cell_phone'           => $this->cell_phone,
            'work_phone'           => $this->work_phone,
            'email'                => $this->email,
            'payment_method_id'    => $this->payment_method_id,
            'voided'               => $this->voided,
            'refunded'             => $this->refunded,
            'void_date'            => $this->void_date,
            'fee'                  => $this->fee ? new ConvenienceFeeResource($this->fee) : null,
            'sub_account_payments' => PaymentSubAccountResource::collection($this->subAccountPayments),
        ];
    }

    /**
     * @return array<string>
     */
    protected static function getRelations(): array
    {
        return ['fee', 'subAccountPayments'];
    }
}
