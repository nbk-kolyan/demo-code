<?php

namespace App\Policies\Payment;

use App\Policies\AbstractPolicy;
use App\Rules\Account\IsNotLegal;
use App\Rules\PaymentIsNotDuplicated;
use App\Rules\SubAccount\CheckSubAccountsMode;
use App\Rules\SubAccount\HasPositiveBalance;
use App\Rules\SubAccount\IsActive;
use App\Rules\SubAccount\IsNotComplete;
use App\Rules\SubAccount\NotPlanAcceptedStatus;
use App\Rules\SubAccount\SubAccountBelongsToAccount;

class AccountAndSubaccountPolicy extends AbstractPolicy
{
    /** {@inheritDoc} */
    public function rules(): array
    {
        $accountId = (int)$this->request->get('account_id');

        return [
            'account_id'   => ['bail', 'required', 'integer', 'exists:account,id', new IsNotLegal()],
            'sub_accounts' => [
                'bail',
                'required',
                'array',
                'min:1',
                new PaymentIsNotDuplicated(),
                new CheckSubAccountsMode(),
            ],
            'sub_accounts.*' => [
                'required',
                'integer',
                new SubAccountBelongsToAccount($accountId),
                new HasPositiveBalance($accountId),
                new IsNotComplete($accountId),
                new IsActive($accountId),
                new NotPlanAcceptedStatus($accountId),
            ],
        ];
    }
}
