<?php

namespace App\Policies\Payment;

use App\Policies\AbstractPolicy;
use App\Rules\Dictionaries\IsValidCountry;
use App\Rules\Dictionaries\IsValidState;
use App\Rules\PaymentAmountMax;
use App\Rules\PaymentAmountMin;

class PaymentCreatePolicy extends AbstractPolicy
{
    /** {@inheritDoc} */
    public function rules(): array
    {
        return [
            'amount' => [
                'bail',
                'required',
                'numeric',
                'regex:/^\d+(\.\d{1,2})?$/',
                new PaymentAmountMin(),
                new PaymentAmountMax(),
            ],
            'address'    => ['sometimes', 'string', 'max:128'],
            'city'       => ['sometimes', 'string', 'max:40'],
            'zip'        => ['sometimes', 'integer', 'digits:5'],
            'phone'      => ['sometimes', 'integer', 'digits:10'],
            'cell_phone' => ['sometimes', 'integer', 'digits:10'],
            'work_phone' => ['sometimes', 'integer', 'digits:10'],
            'email'      => ['sometimes', 'email:filter', 'max:128'],
            'country'    => [
                'bail',
                'required',
                'string',
                'size:2',
                new IsValidCountry(),
            ],
            'state'      => [
                'bail',
                'required',
                'string',
                'size:2',
                new IsValidState(request()->get('country', '')),
            ],
        ];
    }
}
