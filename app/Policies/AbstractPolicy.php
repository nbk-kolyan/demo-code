<?php

namespace App\Policies;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

abstract class AbstractPolicy
{
    protected Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get police rules
     *
     * @return array<mixed>
     */
    abstract public function rules(): array;

    /**
     * @return array<mixed>
     */
    public function messages(): array
    {
        return [];
    }

    /**
     * @return array<mixed>
     */
    public function attributes(): array
    {
        return [];
    }

    public function allows(): Collection
    {
        return collect(Validator::make(
            $this->request->all(),
            $this->rules(),
            $this->messages(),
            $this->attributes()
        )->validate());
    }
}
