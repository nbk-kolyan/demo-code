<?php

namespace App\Policies\PaymentAccount;

use App\Policies\AbstractPolicy;
use App\Rules\PaymentMethod\MethodBelongsToAccount;

/**
 * Class PaymentMethodPolicy
 *
 * @package App\Policies\PaymentAccount
 */
class PaymentMethodPolicy extends AbstractPolicy
{
    /** {@inheritDoc} */
    public function rules(): array
    {
        return [
            'payment_method_id'  => [
                'required',
                'integer',
                'exists:payment_method,id',
                new MethodBelongsToAccount((int)$this->request->get('account_id')),
            ],
        ];
    }
}
