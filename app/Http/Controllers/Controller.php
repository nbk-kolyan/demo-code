<?php

namespace App\Http\Controllers;

use App\Policies\AbstractPolicy;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /** @var array<string> */
    private array $policies = [];

    /**
     * Set an array pf policies to check
     *
     * @param array<string> $policyClasses
     */
    public function policies(array $policyClasses): self
    {
        $this->policies = $policyClasses;

        return $this;
    }

    /**
     * Set a single policy to check
     */
    public function policy(string $policy): self
    {
        return $this->policies([$policy]);
    }

    /**
     * Check policies that were set
     *
     * @throws ValidationException
     */
    public function check(Request $request): Collection
    {
        $dataBlocks = [];

        $validatedData = collect([]);

        if (!$this->policies) {
            return $validatedData;
        }

        $validator = Validator::make($request->all(), []);

        foreach ($this->policies as $name => $policyClass) {
            /** @var AbstractPolicy $policy */
            $policy = new $policyClass($request);

            $validator->addRules($policy->rules());

            $dataBlocks[$name] = array_keys($policy->rules());
        }

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $validatedCollection = collect($validator->validated());

        if (count($this->policies) === 1) {
            return $validatedCollection;
        }

        foreach ($dataBlocks as $key => $block) {
            $validatedData = $validatedData->merge([$key => $validatedCollection->only($block)]);
        }

        return $validatedData;
    }

    protected function noContentResponse(): Response
    {
        return response('', 204);
    }
}
