<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Resources\Payment\PaymentResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PaymentController
 *
 * @package App\Http\Controllers\Pyaments
 */
class PaymentController extends Controller
{
    /**
     * @example api/v1/payments/
     */
    public function index(): JsonResource
    {
        $payments = Payment::paginate(50);

        return PaymentResource::collection($payments);
    }

    /**
     * Display the specified resource
     *
     * @example api/v1/payments/3/
     */
    public function show(int $id): JsonResource
    {
        /** @var Payment $payment */
        $payment = Payment::findOrFail($id);

        return new PaymentResource($payment);
    }
}
