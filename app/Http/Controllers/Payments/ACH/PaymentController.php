<?php

namespace App\Http\Controllers\Payments\ACH;

use App\Facades\Activity;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Policies\Payment\AccountAndSubaccountPolicy;
use App\Policies\Payment\PaymentCreatePolicy;
use App\Policies\PaymentAccount\EcheckCreatePolicy;
use App\Resources\Payment\PaymentResource;
use App\UseCases\Payments\PayWithEcheck;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PaymentController extends Controller
{
    /**
     * Make payment via ACH
     *
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $paymentPolicies = [
            'account'       => AccountAndSubaccountPolicy::class,
            'paymentMethod' => EcheckCreatePolicy::class,
            'payment'       => PaymentCreatePolicy::class,
        ];

        $useCase = new PayWithEcheck($this->policies($paymentPolicies)->check($request));

        $paymentResource = PaymentResource::collection($useCase->perform());

        Activity::logger(Payment::getLogName())
            ->account($request->account_id)
            ->withProperties($paymentResource)
            ->log('made an ACH payment');

        return $paymentResource->response()->setStatusCode(201);
    }
}
