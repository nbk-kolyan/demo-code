<?php

namespace App\Http\Controllers\Payments\PaymentMethod;

use App\Facades\Activity;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Policies\Payment\AccountAndSubaccountPolicy;
use App\Policies\Payment\PaymentCreatePolicy;
use App\Policies\PaymentAccount\PaymentMethodPolicy;
use App\Resources\Payment\PaymentResource;
use App\UseCases\Payments\PayWithPaymentMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PaymentController extends Controller
{
    /**
     * Make payment via Payment Account
     *
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $paymentPolicies = [
            'account'       => AccountAndSubaccountPolicy::class,
            'paymentMethod' => PaymentMethodPolicy::class,
            'payment'       => PaymentCreatePolicy::class,
        ];

        $useCase = new PayWithPaymentMethod($this->policies($paymentPolicies)->check($request));

        $paymentResource = PaymentResource::collection($useCase->perform());

        Activity::logger(Payment::getLogName())
            ->account($request->account_id)
            ->withProperties($paymentResource)
            ->log('made a payment via a Payment Method');

        return $paymentResource->response()->setStatusCode(201);
    }
}
