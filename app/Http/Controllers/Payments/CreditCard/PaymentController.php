<?php

namespace App\Http\Controllers\Payments\CreditCard;

use App\Facades\Activity;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Policies\Payment\AccountAndSubaccountPolicy;
use App\Policies\Payment\PaymentCreatePolicy;
use App\Policies\PaymentAccount\CreditCardCreatePolicy;
use App\Resources\Payment\PaymentResource;
use App\UseCases\Payments\PayWithCreditCard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PaymentController extends Controller
{
    /**
     * Make payment via Credit Card
     *
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $paymentPolicies = [
            'account'       => AccountAndSubaccountPolicy::class,
            'paymentMethod' => CreditCardCreatePolicy::class,
            'payment'       => PaymentCreatePolicy::class,
        ];

        $useCase = new PayWithCreditCard($this->policies($paymentPolicies)->check($request));

        $paymentResource = PaymentResource::collection($useCase->perform());

        Activity::logger(Payment::getLogName())
            ->account($request->account_id)
            ->withProperties($paymentResource)
            ->log('made a CC payment');

        return $paymentResource->response()->setStatusCode(201);
    }
}
