<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use App\Models\PaymentType;
use App\Resources\Payment\PaymentTypeResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PaymentTypeController
 *
 * @package App\Http\Controllers\Payment
 */
class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResource
    {
        $paymentTypes = PaymentType::paginate(50);

        return PaymentTypeResource::collection($paymentTypes);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): JsonResource
    {
        /** @var PaymentType $paymentType */
        $paymentType = PaymentType::findOrFail($id);

        return new PaymentTypeResource($paymentType);
    }
}
