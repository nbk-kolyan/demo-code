<?php

namespace App\Models;

use App\Enums\PaymentStatus;
use App\Services\PaymentTypes\PaymentType;
use App\Traits\DateTimeFormatter;
use App\Traits\LogActivityHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * @property int                 $id
 * @property int                 $account_id
 * @property int                 $payment_plan_id
 * @property float|null          $amount
 * @property float|null          $added_fee
 * @property Carbon              $timestamp
 * @property string|null         $order_id
 * @property string              $auth_code
 * @property string              $comment
 * @property string              $merchant_id
 * @property string              $payment_processor_id
 * @property string|null         $admin_comment
 * @property string              $ip
 * @property string|null         $name
 * @property string|null         $addr
 * @property string|null         $city
 * @property string|null         $state
 * @property string|null         $zip
 * @property string|null         $country
 * @property string|null         $phone
 * @property string|null         $cell_phone
 * @property string|null         $work_phone
 * @property string|null         $email
 * @property bool                $approved
 * @property int                 $payment_method_id
 * @property int                 $status
 * @property bool                $gateway_response_failed
 * @property bool                $voided
 * @property bool                $refunded
 * @property Carbon              $void_date
 * @property Account             $account
 * @property PaymentMethod       $method
 * @property PaymentType         $type
 * @property ConvenienceFee|null $fee
 * @property Collection          $subAccounts
 * @property Collection          $subAccountPayments
 *
 * @property-read string         $statusText
 * @property-read float          $total
 *
 * @method Builder selectRaw(string $expression, array $bindings = [])
 * @method Builder approved()
 */
class Payment extends Model
{
    use LogActivityHelper;
    use HasFactory;
    use DateTimeFormatter;

    public const CREATED_AT = 'timestamp';
    public const UPDATED_AT = 'timestamp';

    /** @var array<string, string> */
    protected $casts = [
        'amount'                  => 'float',
        'approved'                => 'boolean',
        'gateway_response_failed' => 'boolean',
        'voided'                  => 'boolean',
        'refunded'                => 'boolean',
    ];

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'payment';

    /** @var array<string> */
    protected $fillable = [
        'account_id',
        'payment_plan_id',
        'amount',
        'added_fee',
        'timestamp',
        'order_id',
        'auth_code',
        'comment',
        'merchant_id',
        'payment_processor_id',
        'admin_comment',
        'ip',
        'name',
        'addr',
        'city',
        'state',
        'zip',
        'country',
        'phone',
        'cell_phone',
        'work_phone',
        'email',
        'approved',
        'payment_method_id',
        'status',
        'gateway_response_failed',
        'voided',
        'refunded',
        'void_date',
    ];

    /**
     * Title for logs for PaymentMethod model
     */
    protected static string $logName = 'payment_method';

    /**
     * One to one relation with account
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function subAccounts(): HasManyThrough
    {
        return $this->hasManyThrough(
            SubAccount::class,
            PaymentSubAccount::class,
            'payment_id',
            'id',
            'id',
            'sub_account_id'
        );
    }

    public function subAccountPayments(): HasMany
    {
        return $this->hasMany(PaymentSubAccount::class);
    }

    public function fee(): HasOne
    {
        return $this->hasOne(ConvenienceFee::class);
    }

    public function method(): HasOne
    {
        return $this->hasOne(PaymentMethod::class, 'id', 'payment_method_id');
    }

    public function settlementPayment(): HasOne
    {
        return $this->hasOne(SettlementPayment::class);
    }

    public function scopeApproved(Builder $query): Builder
    {
        return $query->where('status', '<>', PaymentStatus::DECLINED);
    }

    public function isFailed(): bool
    {
        return $this->status === PaymentStatus::DECLINED;
    }

    public function isSuccess(): bool
    {
        return !$this->isFailed();
    }

    public function getStatusTextAttribute(?string $value): string
    {
        if ($value) {
            return $value;
        }

        $message = $this->method->type->pending_message;

        if ($this->method->isCreditCard()) {
            $messages = [
                PaymentStatus::APPROVED => $this->method->type->success_message,
                PaymentStatus::DECLINED => $this->method->type->reject_message,
            ];

            $message = $messages[$this->status] ?? $message;
        }

        return $message;
    }

    public function getTotalAttribute(): float
    {
        return (float)($this->amount + ($this->fee ? $this->fee->amount : 0));
    }
}
