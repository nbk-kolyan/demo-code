<?php

namespace App\Models;

use App\Traits\LogActivityHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $name
 * @property string $short_name
 * @property string $pending_message
 * @property string $success_message
 * @property string $reject_message
 */
class PaymentType extends Model
{
    use LogActivityHelper;

    /**
     * Should not be timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'payment_type';

    protected $fillable = [
        'name',
        'short_name',
        'pending_message',
        'success_message',
        'reject_message',
    ];

    /**
     * Title for logs for PaymentType model
     */
    protected static string $logName = 'payment_type';
}
