<?php

namespace App\UseCases\Payments;

use App\Exceptions\SubAccountsHaveActiveSettlement;
use App\Facades\NegotiatorSettings;
use App\Helpers\TransferObject;
use App\Models\Account;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\Settlement;
use App\Services\PaymentModes\PaymentFactory;

abstract class AbstractPay
{
    /**
     * @var array<mixed>
     */
    protected array $paymentData;

    /**
     * @var array<mixed>
     */
    protected array $accountData;
    private bool $payingForSettlement;

    /**
     * @param array<mixed> $paymentData
     * @param array<mixed> $accountData
     */
    public function __construct(array $paymentData, array $accountData, bool $payingForSettlement = false)
    {
        $this->paymentData         = $paymentData;
        $this->accountData         = $accountData;
        $this->payingForSettlement = $payingForSettlement;
    }

    /**
     * @return array<int, Payment>
     */
    public function perform(): array
    {
        /** @var Account $account */
        $account = Account::getForPayment($this->accountData['account_id'], $this->accountData['sub_accounts']);

        if (
            !$this->payingForSettlement
            && Settlement::hasSubAccounts($account->subAccounts)->paid(false)->exists()
        ) {
            throw new SubAccountsHaveActiveSettlement();
        }

        $paymentMethod = $this->getPaymentMethod();

        $merchantMode = PaymentFactory::getPayment();

        return $merchantMode->pay(new TransferObject([
            'account'               => $account,
            'account_id'            => $account->id,
            'sub_accounts'          => $account->subAccounts->fresh()->toArray(),
            'account_apply_fee'     => $account->apply_fee,
            'origin_client_id'      => NegotiatorSettings::get('id'),
            'merchant_id'           => NegotiatorSettings::get('merchant_id'),
            'payment_method_id'     => $paymentMethod->id,
            'payment_account_id'    => $paymentMethod->payment_account_id,
            'billing_name'          => $paymentMethod->name,
            'payment_type'          => $paymentMethod->payment_type,
            'payment_plan_id'       => $this->paymentData['payment_plan_id'] ?? 0,
            'amount'                => $this->paymentData['amount'],
            'cust_state'            => $this->paymentData['state'],
            'cust_country'          => $this->paymentData['country'],
            'cust_city'             => $this->paymentData['city'] ?? '',
            'cust_zip'              => $this->paymentData['zip'] ?? '',
            'cust_email'            => $this->paymentData['email'] ?? '',
            'cust_addr'             => $this->paymentData['address'] ?? '',
            'phone'                 => $this->paymentData['phone'] ?? '',
            'cell_phone'            => $this->paymentData['cell_phone'] ?? '',
            'work_phone'            => $this->paymentData['work_phone'] ?? '',
            'email'                 => $this->paymentData['email'] ?? '',
            'cust_comment'          => '',
            'cust_acc_number'       => $account->account_number,
            'cust_date_of_birth'    => $account->info ? $account->info->birth_date : '',
            'cust_name'             => $account->info ? $account->info->full_name : '',
            'cust_first_name'       => $account->info ? $account->info->first_name : '',
            'cust_last_name'        => $account->info ? $account->info->last_name : '',
            'cust_id'               => $account->service()->generateCustomerId(),
            'cust_date'             => $account->service()->generateCustomerDate(),
            'custom'                => $account->service()->generateCustomFields(),
            'settlement_id'         => $this->paymentData['settlement_id'] ?? null,
        ]));
    }

    /**
     * Gat payment method
     */
    abstract protected function getPaymentMethod(): PaymentMethod;
}
