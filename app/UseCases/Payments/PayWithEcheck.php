<?php

namespace App\UseCases\Payments;

use App\Facades\Activity;
use App\Interfaces\IGateway;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\UseCases\PaymentMethods\CreateEcheckPaymentMethod;
use Illuminate\Support\Collection;

class PayWithEcheck extends AbstractPay
{
    private Collection $ACHData;

    public function __construct(Collection $validatedData)
    {
        $accountData = $validatedData->get('account');

        $this->ACHData = $validatedData->get('paymentMethod')->merge($accountData);

        parent::__construct($validatedData->get('payment')->toArray(), $accountData->toArray());
    }

    protected function getPaymentMethod(): PaymentMethod
    {
        /** @var IGateway $paymentMethodService */
        $paymentMethodService = app(IGateway::class);

        $useCase = new CreateEcheckPaymentMethod($paymentMethodService, $this->ACHData, false);

        $paymentMethodACH = $useCase->perform();

        Activity::logger(Payment::getLogName())
            ->withProperties($paymentMethodACH)
            ->log('created an ACH payment method');

        return $paymentMethodACH;
    }
}
