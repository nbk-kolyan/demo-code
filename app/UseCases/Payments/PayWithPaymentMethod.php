<?php

namespace App\UseCases\Payments;

use App\Models\PaymentMethod;
use Illuminate\Support\Collection;

class PayWithPaymentMethod extends AbstractPay
{
    private Collection $paymentMethodData;

    public function __construct(Collection $validatedData, bool $payingForSettlement = false)
    {
        $accountData = $validatedData->get('account');

        $this->paymentMethodData = $validatedData->get('paymentMethod');

        parent::__construct($validatedData->get('payment')->toArray(), $accountData->toArray(), $payingForSettlement);
    }

    protected function getPaymentMethod(): PaymentMethod
    {
        return PaymentMethod::with('type')->findOrFail((int)$this->paymentMethodData->get('payment_method_id'));
    }
}
