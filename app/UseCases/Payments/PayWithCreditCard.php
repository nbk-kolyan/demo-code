<?php

namespace App\UseCases\Payments;

use App\Facades\Activity;
use App\Interfaces\IGateway;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\UseCases\PaymentMethods\CreateCreditCardPaymentMethod;
use Illuminate\Support\Collection;

class PayWithCreditCard extends AbstractPay
{
    private Collection $creditCardData;

    public function __construct(Collection $validatedData)
    {
        $accountData = $validatedData->get('account');

        $this->creditCardData = $validatedData->get('paymentMethod')->merge($accountData);

        parent::__construct($validatedData->get('payment')->toArray(), $accountData->toArray());
    }

    protected function getPaymentMethod(): PaymentMethod
    {
        /** @var IGateway $paymentMethodService */
        $paymentMethodService = app(IGateway::class);

        $useCase = new CreateCreditCardPaymentMethod($paymentMethodService, $this->creditCardData, false);

        $paymentMethodCC = $useCase->perform();

        Activity::logger(Payment::getLogName())
            ->withProperties($paymentMethodCC)
            ->log('created a CC payment method');

        return $paymentMethodCC;
    }
}
