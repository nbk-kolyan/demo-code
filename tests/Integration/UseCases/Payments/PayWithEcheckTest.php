<?php

namespace Tests\Integration\UseCases\Payments;

use App\Enums\EcheckAccountType;
use App\Enums\PaymentMethodId;
use App\Enums\SubAccountState;
use App\Facades\NegotiatorSettings;
use App\UseCases\Payments\PayWithEcheck;
use Illuminate\Support\Collection;

/**
 * @coversDefaultClass \App\UseCases\Payments\PayWithEcheck
 *
 */
class PayWithEcheckTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        NegotiatorSettings::set('adjust_balance', true);
        NegotiatorSettings::set('dont_mark_accounts_complete', false);

        $this->stubRequest();
    }

    /**
     * @covers ::perform
     */
    public function testPerform(): void
    {
        $subAccounts = $this->prepareSubAccounts()
            ->random(3);

        $payload = $this->buildPayload($subAccounts);

        $payments = (new PayWithEcheck($payload))->perform();

        $this->assertCount(1, $payments);
        $this->assertEcheckInDatabase($payload, $payments[0]->payment_method_id);
        $this->assertPaymentsInDatabase($payload, [
            $payments[0]->id => $subAccounts->sum('current_balance'),
        ]);
        $this->assertSubAccountsPaid($payments[0], $subAccounts);
        $this->assertSubAccountsStatus(array_fill_keys(
            $subAccounts->pluck('id')->toArray(),
            SubAccountState::COMPLETE
        ));
    }

    /**
     * @covers ::perform
     */
    public function testPerformSubAccountsNotMarkedAsComplete(): void
    {
        $subAccounts = $this->prepareSubAccounts();

        $payload = $this->buildPayload($subAccounts);

        NegotiatorSettings::set('dont_mark_accounts_complete', true);

        $payments = (new PayWithEcheck($payload))->perform();

        $this->assertCount(1, $payments);
        $this->assertEcheckInDatabase($payload, $payments[0]->payment_method_id);
        $this->assertPaymentsInDatabase($payload, [
            $payments[0]->id => $subAccounts->sum('current_balance'),
        ]);
        $this->assertSubAccountsPaid($payments[0], $subAccounts);
        $this->assertSubAccountsStatus(array_fill_keys(
            $subAccounts->pluck('id')->toArray(),
            SubAccountState::INCOMPLETE
        ));
    }

    /**
     * @covers ::perform
     */
    public function testPerformSomeSubAccounts(): void
    {
        $subAccounts = $this->prepareSubAccounts()
            ->slice(1, 3);

        $payload = $this->buildPayload($subAccounts);

        $payments = (new PayWithEcheck($payload))->perform();

        $this->assertCount(1, $payments);
        $this->assertEcheckInDatabase($payload, $payments[0]->payment_method_id);
        $this->assertPaymentsInDatabase($payload, [
            $payments[0]->id => $subAccounts->sum('current_balance'),
        ]);
        $this->assertSubAccountsPaid($payments[0], $subAccounts);
        $this->assertSubAccountsStatus([
            $subAccounts[1]->id => SubAccountState::COMPLETE,
            $subAccounts[2]->id => SubAccountState::COMPLETE,
            $subAccounts[3]->id => SubAccountState::COMPLETE,
        ]);
    }

    /**
     * @covers ::perform
     */
    public function testPerformMultiplePayments(): void
    {
        $subAccounts = $this->prepareSubAccounts(5, 2)
            ->slice(1, 3);

        $payload = $this->buildPayload($subAccounts);

        $payments = (new PayWithEcheck($payload))->perform();

        $firstPayment = collect([$subAccounts[1], $subAccounts[3]]);
        $secondPayment = collect([$subAccounts[2]]);

        $this->assertCount(2, $payments);
        $this->assertEcheckInDatabase($payload, $payments[0]->payment_method_id);
        $this->assertPaymentsInDatabase(
            $payload,
            [
                $payments[0]->id => $firstPayment->sum('current_balance'),
                $payments[1]->id => $secondPayment->sum('current_balance'),
            ]
        );
        $this->assertSubAccountsPaid($payments[0], $firstPayment);
        $this->assertSubAccountsPaid($payments[1], $secondPayment);
        $this->assertSubAccountsStatus([
            $subAccounts[1]->id => SubAccountState::COMPLETE,
            $subAccounts[2]->id => SubAccountState::COMPLETE,
            $subAccounts[3]->id => SubAccountState::COMPLETE,
        ]);
    }

    protected function buildPayload(Collection $subAccounts, ?int $amount = null): Collection
    {
        return parent::preparePayload($subAccounts, $amount, [
            'account_number' => self::$faker->regexify('\d{9}'),
            'routing_number' => self::$faker->regexify('[1-9]\d{8}'),
            'account_type'   => self::$faker->randomElement(EcheckAccountType::$all),
            'name'           => self::$faker->name,
        ]);
    }

    protected function assertEcheckInDatabase(Collection $payload, int $paymentMethodId): void
    {
        $echeckData = $payload->get('paymentMethod');

        $this->seeInDatabase(
            'payment_method',
            [
                'id'               => $paymentMethodId,
                'payment_type_id'  => PaymentMethodId::ACH,
                'name'             => $echeckData->get('name'),
                'last4'            => substr($echeckData->get('account_number'), -4),
                'saved'            => false,
                'ach_account_type' => $echeckData->get('account_type'),
            ]
        );
    }
}
