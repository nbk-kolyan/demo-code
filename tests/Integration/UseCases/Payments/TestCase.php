<?php

namespace Tests\Integration\UseCases\Payments;

use App\Models\Payment;
use App\Services\RequestId;
use App\Models\Account;
use App\Models\Client;
use App\Models\SubAccount;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\Integration\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    /**
     * @return Request|MockObject
     */
    protected function stubRequest(): MockObject
    {
        $request = $this->mockService(Request::class);

        $request->expects($this->any())
            ->method('ip')
            ->willReturn(self::$faker->ipv4);
        $request->expects($this->any())
            ->method('header')
            ->with(RequestId::PARENT_REQUEST_ID_HEADER)
            ->willReturn(self::$faker->uuid);

        request()->ip();

        return $request;
    }

    /**
     * @param int $subAccountsNumber
     * @param int $clientsNumber
     *
     * @return Collection<SubAccount>
     */
    protected function prepareSubAccounts(int $subAccountsNumber = 5, int $clientsNumber = 1): Collection
    {
        $clientIds = Client::factory($clientsNumber)
            ->create()
            ->map(
                fn (Client $client) => ['client_id' => $client->id]
            );

        $subAccounts = SubAccount::factory($subAccountsNumber)
            ->payable()
            ->state(new Sequence(...$clientIds));

        /** @var Account $account */
        $account = Account::factory()
            ->payable()
            ->has($subAccounts)
            ->create();

        return $account->subAccounts;
    }

    protected function preparePayload(Collection $subAccounts, ?int $amount, array $paymentMethodData): Collection
    {
        $amount ??= $subAccounts->sum('current_balance');

        return collect([
            'paymentMethod' => collect($paymentMethodData),
            'account' => collect([
                'account_id'     => $subAccounts->first()->account_id,
                'sub_accounts'   => $subAccounts->pluck('id')->toArray(),
            ]),
            'payment' => collect([
                'amount'         => round($amount, 2),
                'state'          => 'AZ',
                'country'        => 'US',
                'address'        => self::$faker->streetAddress,
            ]),
        ]);
    }

    /**
     * @param Collection       $payload
     * @param array<int,float> $payments
     */
    protected function assertPaymentsInDatabase(Collection $payload, array $payments): void
    {
        $paymentData = [
            'ip'      => $this->getService(Request::class)->ip(),
            'name'    => ' ',
            'country' => Arr::get($payload, 'payment.country'),
            'state'   => Arr::get($payload, 'payment.state'),
            'addr'    => Arr::get($payload, 'payment.address'),
        ];

        foreach ($payments as $paymentId => $amount) {
            $this->seeInDatabase(
                'payment',
                $paymentData + [
                    'id' => $paymentId,
                    'amount' => $amount,
                ]
            );
        }
    }

    /**
     * @param array<int,Collection<SubAccount>> $payments key is payment_id & value is list of SubAccounts
     * @param array<int,float>                  $amounts  list of custom amounts paid for sub account
     *                                                    if custom amount not provided, current balance is taken
     */
    protected function assertSubAccountsPaid(Payment $payment, Collection $subAccounts, array $amounts = []): void
    {
        /** @var SubAccount $subAccount */
        foreach ($subAccounts as $subAccount) {
            $paid = $amounts[$subAccount->id] ?? $subAccount->current_balance;

            $this->seeInDatabase(
                'sub_account',
                [
                    'id'              => $subAccount->id,
                    'current_balance' => $subAccount->current_balance - $paid,
                ]
            );
            $this->seeInDatabase(
                'payment_sub_account',
                [
                    'payment_id'     => $payment->id,
                    'sub_account_id' => $subAccount->id,
                    'amount'         => $paid,
                ]
            );
        }
    }

    /**
     *
     * @param array<int,int> $statuses keypair of sub account id & status
     */
    protected function assertSubAccountsStatus(array $statuses): void
    {
        return;

        foreach ($statuses as $subAccountId => $status) {
            $this->seeInDatabase(
                'sub_account',
                [
                    'id'     => $subAccountId,
                    'status' => $status,
                ]
            );
        }
    }
}
