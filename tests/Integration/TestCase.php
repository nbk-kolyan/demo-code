<?php

namespace Tests\Integration;

use App\API\Internal\FakeGateway\FakeGateway;
use App\Facades\Negotiator;
use App\Interfaces\IGateway;
use App\Interfaces\IPaymentGateway;
use Tests\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use ServicesTrait;

    protected function setUp(): void
    {
        parent::setUp();

        Negotiator::init(self::NEGOTIATOR_ID);

        $this->substituteService(IGateway::class, new FakeGateway(''));
        $this->substituteService(IPaymentGateway::class, new FakeGateway(''));
    }
}
