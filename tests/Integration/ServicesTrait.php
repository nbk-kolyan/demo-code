<?php

namespace Tests\Integration;

use Laravel\Lumen\Application;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @property-read Application $app
 *
 * @mixin \Tests\TestCase
 */
trait ServicesTrait
{
    /** @return mixed */
    protected function getService(string $serviceId)
    {
        return $this->app->make($serviceId);
    }

    protected function substituteService(string $serviceId, object $stub): void
    {
        $this->app->instance($serviceId, $stub);
    }

    protected function mockService(string $serviceId, ?string $mockClass = null): MockObject
    {
        $mock = $this->createMock($mockClass ?? $serviceId);

        $this->substituteService($serviceId, $mock);

        return $mock;
    }
}
