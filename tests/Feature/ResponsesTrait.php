<?php

namespace Tests\Feature;

use App\DataTransferObjects\BulkItemProcessingError;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Throwable;

/**
 * @noinspection PhpFullyQualifiedNameUsageInspection
 *
 * @property \Illuminate\Testing\TestResponse|\Illuminate\Http\Response $response
 *
 * @mixin \Laravel\Lumen\Testing\Concerns\MakesHttpRequests
 * @mixin \PHPUnit\Framework\TestCase
 */
trait ResponsesTrait
{
    /**
     * @param mixed $entity
     *
     * @return $this
     */
    protected function assertResourceResponse(string $resourceClass, $entity, int $statusCode = 200): self
    {
        if ($entity !== null) {
            $data = (new $resourceClass($entity))->response()->getData(true)['data'];
        } else {
            $data = [];
        }

        return $this->assertResponse($statusCode, [
            'data' => $data,
        ]);
    }

    /**
     * @param array<mixed> $payload
     *
     * @return $this
     */
    protected function assertErrorResponse(int $statusCode, string $error, array $payload = []): self
    {
        return $this->assertResponse(
            $statusCode,
            $payload + ['error'  => $error]
        );
    }

    /**
     * @param array<mixed> $fields
     *
     * @return $this
     */
    protected function assertValidationErrorResponse(array $fields): self
    {
        return $this->assertResponse(
            422,
            [
                'error'  => 'Request contains invalid data.',
                'fields' => $fields
            ]
        );
    }

    protected function assertEmptyResponse()
    {
        $this->assertSame(204, $this->response->status(), $this->response->content());
        $this->assertEmpty($this->response->content());
    }

    protected function assertListResponse(string $resourceClass, Collection $collection, int $statusCode = 200): self
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $resource = $resourceClass::collection($collection)->response()->getData(true);

        return $this->assertResponse($statusCode, [
            'data'    => $resource['data'],
        ]);
    }

    protected function assertPaginatedResponse(
        string $resourceClass,
        LengthAwarePaginator $paginator,
        int $statusCode = 200
    ): self {
        /** @noinspection PhpUndefinedMethodInspection */
        $resource         = $resourceClass::collection($paginator)->response()->getData(true);
        $paginationFields = $this->buildPaginationFields($resource);

        return $this->assertResponse($statusCode, [
            'data'    => $resource['data'],
        ] + $paginationFields);
    }

    protected function assertFilteredResponse(
        string $resourceClass,
        Collection $expectedItems,
        int $itemsPerPage = 50,
        int $statusCode = 200
    ): self {
        /** @var Model $item */
        $item = $expectedItems->first();

        $modelClass = get_class($item);
        $primaryKey = $item->getKeyName();

        $paginator = $modelClass::whereIn($primaryKey, $expectedItems->pluck($primaryKey)->toArray())
            ->paginate($itemsPerPage);

        return $this->assertPaginatedResponse($resourceClass, $paginator, $statusCode);
    }

    /**
     * @param array<Model|BulkItemProcessingError> $entities
     */
    protected function assertBulkResponse(array $entities): self
    {
        $response = [];

        foreach ($entities as $idx => $entity) {
            if ($entity instanceof Model) {
                $response[$idx] = [$entity->getKeyName() => $entity->getKey()];
            } else {
                $response[$idx] = ['error' => $entity->getMessage(), 'data' => $entity->getPayload()];
            }
        }

        return $this->assertResponse(200, ['data' => $response]);
    }

    /**
     * @param array<string,mixed> $resource
     *
     * @return array<mixed>
     */
    private function buildPaginationFields(array $resource): array
    {
        if (!isset($resource['meta'])) {
            $this->fail(__FUNCTION__ . ' failed. Cant find pagination "meta" field');
        }

        return [
            'meta' => Arr::only(
                $resource['meta'],
                [
                    'current_page',
                    'from',
                    'last_page',
                    'per_page',
                    'to',
                    'total',
                ]
            ),
        ];
    }

    /**
     * @param array<mixed> $expectedBody
     *
     * @return $this
     */
    private function assertResponse(int $statusCode, array $expectedBody): self
    {
        try {
            $actualBody = $this->response->json();
        } catch (Throwable $exception) {
            $this->fail($exception->getMessage());
        }

        $this->assertSame($statusCode, $this->response->status(), $this->response->content());
        $this->assertEquals($expectedBody, $actualBody ?? []);

        return $this;
    }
}
