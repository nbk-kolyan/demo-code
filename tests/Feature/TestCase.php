<?php

namespace Tests\Feature;

use App\API\Internal\FakeGateway\FakeGateway;
use App\Interfaces\IGateway;
use App\Interfaces\IPaymentGateway;
use App\Facades\Negotiator;
use App\Resources\AbstractResource;
use Tests\Integration\ServicesTrait;
use Tests\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use ResponsesTrait;
    use ServicesTrait;

    protected function setUp(): void
    {
        parent::setUp();

        $this->substituteService(IGateway::class, new FakeGateway(''));
        $this->substituteService(IPaymentGateway::class, new FakeGateway(''));

        Negotiator::init(self::NEGOTIATOR_ID);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        AbstractResource::clearCachedResources();
    }

    /**
     * @inheritDoc
     *
     * @param array<mixed> $params
     */
    public function get($uri, array $headers = [], array $params = [])
    {
        $server = $this->transformHeadersToServerVars($headers);

        $this->call('GET', $uri, $params, [], [], $server);

        return $this;
    }
}
