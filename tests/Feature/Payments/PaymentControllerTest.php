<?php

namespace Tests\Feature\Payments;

use App\Models\Payment;
use App\Resources\Payment\PaymentResource;
use Tests\Feature\TestCase;

/**
 * @coversDefaultClass \App\Http\Controllers\Payments\PaymentController
 */
class PaymentControllerTest extends TestCase
{
    /**
     * @covers ::index
     */
    public function testIndex(): void
    {
        $this->get($this->route());

        $this->assertPaginatedResponse(PaymentResource::class, Payment::paginate(50));
    }

    /**
     * @covers ::show
     */
    public function testShow(): void
    {
        /** @var Payment $payment */
        $payment = Payment::factory()
            ->withFee()
            ->withPaymentSubAccount()
            ->create();

        $this->get($this->route($payment->id));

        $this->assertResourceResponse(PaymentResource::class, $payment);
    }

    /**
     * @covers ::show
     */
    public function testShowNotFound(): void
    {
        $this->get($this->route(3141));

        $this->assertErrorResponse(404, 'Does not exist any payment with the specified identifier');
    }

    private function route(int $id = null): string
    {
        return '/intelligent-negotiator/v1/payments/' . $id;
    }
}
