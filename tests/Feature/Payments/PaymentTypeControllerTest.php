<?php

namespace Tests\Feature\Payments;

use App\Models\PaymentType;
use App\Resources\Payment\PaymentTypeResource;
use Tests\Feature\TestCase;

/**
 * @coversDefaultClass \App\Http\Controllers\Payments\PaymentTypeController
 */
class PaymentTypeControllerTest extends TestCase
{
    /**
     * @covers ::index
     */
    public function testIndex(): void
    {
        $this->get($this->route());

        $this->assertPaginatedResponse(PaymentTypeResource::class, PaymentType::paginate(50));
    }

    /**
     * @covers ::show
     */
    public function testShow(): void
    {
        $this->get($this->route(1));

        $this->assertResourceResponse(PaymentTypeResource::class, PaymentType::find(1));
    }

    /**
     * @covers ::show
     */
    public function testShowNotFound(): void
    {
        $this->get($this->route(3141));

        $this->assertErrorResponse(404, 'Does not exist any payment type with the specified identifier');
    }

    private function route(int $id = null): string
    {
        return '/intelligent-negotiator/v1/payment-types/' . $id;
    }
}
