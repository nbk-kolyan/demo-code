<?php

namespace Tests\Feature\Payments\PaymentMethod;

use App\Models\Account;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\SubAccount;
use App\Resources\Payment\PaymentResource;
use Tests\Feature\TestCase;

/**
 * @coversDefaultClass \App\Http\Controllers\Payments\PaymentMethod\PaymentController
 */
class PaymentControllerTest extends TestCase
{
    /**
     * @covers ::store
     *
     * @throws \Throwable
     */
    public function testStore(): void
    {
        $this->post('/intelligent-negotiator/v1/payments/payment-method', $this->buildPayload());

        $paymentIds = $this->response->json('data.*.id');

        $this->assertListResponse(PaymentResource::class, Payment::findMany($paymentIds), 201);
    }

    /**
     * @covers ::store
     */
    public function testStoreValidationOnMissingCountry(): void
    {
        $payload = $this->buildPayload();
        unset($payload['country']);

        $this->post('/intelligent-negotiator/v1/payments/payment-method', $payload);

        $this->assertValidationErrorResponse([
            'country' => ['The country field is required.'],
            'state'   => ['The selected state is invalid.'],
        ]);
    }

    /**
     * @return array<string,mixed>
     */
    protected function buildPayload(): array
    {
        /** @var Account $account */
        $account       = Account::factory()->payable()->create();
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = PaymentMethod::factory()->create(['account_id' => $account]);
        $subAccounts   = SubAccount::factory()->count(3)
            ->payable()
            ->create([
                'account_id' => $account->id,
            ]);

        return [
            'account_id'        => $account->id,
            'sub_accounts'      => $subAccounts->pluck('id')->toArray(),
            'amount'            => (int)$subAccounts->avg('current_balance'),
            'state'             => 'AZ',
            'country'           => 'US',
            'address'           => self::$faker->streetAddress,
            'payment_method_id' => $paymentMethod->id,
        ];
    }
}
