# demo-code

example of code 

This is the example of the code, I wrote in current project.

This is implementation of extracting business logic from legacy spaghetti code to REST API.
It will allow giving access to some clients, for integration. 
Also, it will allow refactoring frontend application to use API endpoints (extract business logic in one place), 
make it more reliable(covered by tests) and easy to support(better application architecture)

It's payment-related endpoints(Controllers). This code is not working - it requires our system libraries, and docker integration, but for example - it's ok

Policy - in our case, its Requests in Laravel , their responsibility is to perform validation of request [Form Request Validation](https://laravel.com/docs/9.x/validation#form-request-validation)

We use UseCase as Actions in Laravel, all business logic is  in the use-case [Laravel Actions](https://laravelactions.com/)

All endpoints should return Resources , similar to [API Resources](https://laravel.com/docs/9.x/eloquent-resources)

And tests - consists from feature tests, for testing that endpoint returns corrects Resource with correct structure.
And from integration test - for testing that after specific action we actually have changes in Database